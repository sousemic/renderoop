treeRender
treePrint: aPosition
	^ (self makeSpace: aPosition) , '- ' , self treeName, (self treePrintContent: aPosition)