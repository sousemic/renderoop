render
makeSpace: aPosition
	| spaces |
	spaces := ''.
	1 to: aPosition * 2 do: [ :i | spaces := spaces , ' ' ].
	^ spaces