treeRender
treePrintContent: aPosition
	| content |
	content := ''.
	nodes do: [ :each | content := content , (each treePrint: aPosition + 1) ].
	^'
',content