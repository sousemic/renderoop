tests
testTreeRendererDocument
	| doc  result|
	doc := TRDocument new
  nodes: {
    TRHeading new nodes: {
      TRText new text: 'Welcome'
    }.
    TRSection new
      nodes: {
        TRSection new
          nodes: {
            TRHeading new nodes: {
              TRText new text: 'Lorem'
            }.
            TRParagraph new nodes: {
              TRText new text: 'Lorem ipsum'.
              TRText new text: '(etc...)'
            }
          }.
        TRParagraph new nodes: {
          TRText new text: 'Hello'.
        }
      }.
    TRText new text: 'a note'
  }.

result := 
'- document
  - heading
    - text ("Welcome")
  - section
    - section
      - heading
        - text ("Lorem")
      - paragraph
        - text ("Lorem ipsum")
        - text ("(etc...)")
    - paragraph
      - text ("Hello")
  - text ("a note")
'.

self assert: ((TRTreeRenderer convert: doc) = result).

