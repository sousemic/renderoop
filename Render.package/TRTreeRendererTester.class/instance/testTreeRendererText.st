tests
testTreeRendererText
	| doc  result|
	doc := 
      TRText new text: 'Welcome'.
result:=
'- text ("Welcome")
'.

self assert: ((TRTreeRenderer convert: doc) = result).

