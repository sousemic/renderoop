tests
testTreeRendererHeading
	| doc  result|
	doc := 
    TRHeading new nodes: {
      TRText new text: 'Welcome'
    }.
result:=
'- heading
  - text ("Welcome")
'.

self assert: ((TRTreeRenderer convert: doc) = result).

