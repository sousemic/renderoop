tests
testTreeRendererDParagraph
	| doc  result|
	doc := TRParagraph new nodes: {
              TRText new text: 'Lorem ipsum'.
              TRText new text: '(etc...)'
            }  .      

result := 
'- paragraph
  - text ("Lorem ipsum")
  - text ("(etc...)")
'.

self assert: ((TRTreeRenderer convert: doc) = result).

