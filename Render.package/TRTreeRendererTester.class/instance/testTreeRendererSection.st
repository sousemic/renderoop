tests
testTreeRendererSection
	| doc  result|
	doc := 
        TRSection new
          nodes: {
              TRText new text: 'Lorem'
          }.


result := 
'- section
  - text ("Lorem")
'.

self assert: ((TRTreeRenderer convert: doc) = result).

