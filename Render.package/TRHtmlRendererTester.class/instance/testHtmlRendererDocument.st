tests
testHtmlRendererDocument
	| doc  result|
	doc := TRDocument new
  nodes: {
    TRHeading new nodes: {
      TRText new text: 'Welcome'
    }.
    TRSection new
      nodes: {
        TRSection new
          nodes: {
            TRHeading new nodes: {
              TRText new text: 'Lorem'
            }.
            TRParagraph new nodes: {
              TRText new text: 'Lorem ipsum'.
              TRText new text: '(etc...)'
            }
          }.
        TRParagraph new nodes: {
          TRText new text: 'Hello'.
        }
      }.
    TRText new text: 'a note'
  }.

result := 
'<html>
  <body>
    <h2>
      Welcome
    </h2>
    <div class="section">
      <div class="section">
        <h2>
          Lorem
        </h2>
        <p>
          Lorem ipsum
          (etc...)
        </p>
      </div>
      <p>
        Hello
      </p>
    </div>
    a note
  </body>
</html>'.

self assert: ((TRHtmlRenderer convert: doc) = result).

