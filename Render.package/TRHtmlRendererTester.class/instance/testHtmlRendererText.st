tests
testHtmlRendererText
	| doc  result|
	doc := 
      TRText new text: 'Welcome'.
result:=
'
Welcome'.

self assert: ((TRHtmlRenderer convert: doc) = result).

