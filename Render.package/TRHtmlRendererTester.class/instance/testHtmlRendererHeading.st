tests
testHtmlRendererHeading
	| doc  result|
	doc := 
    TRHeading new nodes: {
      TRText new text: 'Welcome'
    }.
result:=
'
<h2>
  Welcome
</h2>'.

self assert: ((TRHtmlRenderer convert: doc) = result).

