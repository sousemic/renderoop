tests
testHtmlRendererDParagraph
	| doc  result|
	doc := TRParagraph new nodes: {
              TRText new text: 'Lorem ipsum'.
              TRText new text: '(etc...)'
            }  .      

result := 
'
<p>
  Lorem ipsum
  (etc...)
</p>'.

self assert: ((TRHtmlRenderer convert: doc) = result).

