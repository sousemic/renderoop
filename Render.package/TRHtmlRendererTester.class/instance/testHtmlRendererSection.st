tests
testHtmlRendererSection
	| doc  result|
	doc := 
        TRSection new
          nodes: {
              TRText new text: 'Lorem'
          }.


result := 
'
<div class="section">
  Lorem
</div>'.

self assert: ((TRHtmlRenderer convert: doc) = result).

